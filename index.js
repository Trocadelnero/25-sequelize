const { Sequelize, QueryTypes } = require('sequelize');

if (process.env.NODE_ENV !== 'production') {
    require('dotenv').config();
}

const sequelize = new Sequelize( {
    database: process.env.DB_DATABASE,
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    host: process.env.DB_HOST,
    dialect: process.env.DB_DIALECT
});

async function connect() {
    try {
        await sequelize.authenticate();
        console.log('Connection established successfully');

        const countries = await sequelize.query('SELECT * FROM country', 
        {type: QueryTypes.SELECT
        });

        const cities = await sequelize.query('SELECT * FROM city', 
        { type: QueryTypes.SELECT });


        const languages = await sequelize.query('SELECT * FROM countrylanguage', 
        {type: QueryTypes.SELECT
        });

        //[0] added to not spam console
        console.log(countries[0], cities[0], languages[0]);
        await sequelize.close();   
    }
    catch(e) {
        console.error(e);
        
    }
};

connect();






