# Task 25 - Basic Sequelize App
---
## Instructions

- Create a basic Node app
- Install Sequelize and dotenv
- Connect to the World database (Sample Data from MySQL Installation) using Sequelize
- Use process.env for database credentials
- Write 3 functions that use Sequelize Raw Queries to:
1. Get all the countries
2. Get all the cities
3. Get all the languages